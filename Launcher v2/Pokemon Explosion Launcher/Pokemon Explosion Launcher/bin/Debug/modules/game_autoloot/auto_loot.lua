local protocolGame
local AlOpCode = 33
local AlShortcut = "Alt+A"
local playerMaxAlItens = 0

local AlWindow = nil
local listItens = nil
local listPlayerItens = nil
local searchEditText = nil
local alStateBtn = nil
local cleanPlayerItensBtn = nil
local playerItensLabel = nil

local serverInfo ={
  alState = 0,
  maxAlItens = 0,
  itens = {},
  alItens = {}
}

local listItemViews = {}

function init()
  connect(g_game, { onGameEnd = offline })
  ProtocolGame.registerExtendedOpcode(AlOpCode, function(protocol, opcode, buffer) loadItens(buffer) end)

  btnToggleAl = modules.client_topmenu.addRightGameToggleButton('alButton', tr('Auto-Loot') .. '('..AlShortcut..')', '/images/topbuttons/loot', toggle)
  g_keyboard.bindKeyDown(AlShortcut, toggle)
  
  setupViews()
end

function setupViews()
  AlWindow = g_ui.displayUI('auto_loot')
  AlWindow:hide()

  listItens = AlWindow:getChildById('itensAutoLoot')
  listPlayerItens = AlWindow:getChildById('playerItens')
  searchEditText = AlWindow:getChildById('search')
  alStateBtn = AlWindow:getChildById('autoLootStateBtn')
  cleanPlayerItensBtn = AlWindow:getChildById('cleanPlayerItensBtn')
  playerItensLabel = AlWindow:getChildById('playerItensLabel')
end

function terminate()
  disconnect(g_game, { onGameEnd = offline })
  ProtocolGame.unregisterExtendedOpcode(AlOpCode)

  AlWindow:destroy()
end

function toggle()
	if AlWindow:isVisible() then
		AlWindow:hide()
	else
		onOpenAutoLoot()
	end
end

function onOpenAutoLoot()
	AlWindow:show()
	AlWindow:raise()
  AlWindow:focus()
  protocolGame = g_game.getProtocolGame()
  protocolGame:sendExtendedOpcode(AlOpCode, "loadAutoLoot")
end

function offline()
  clearLists()
  AlWindow:hide()
end

function loadItens(buffer)
  local receivedProtocol = loadstring("return ".. buffer)()
  if receivedProtocol[3] == "itens" then
    listItemViews = {}
    serverInfo.alState = Protocol_read(receivedProtocol)
    serverInfo.maxAlItens = Protocol_read(receivedProtocol)
    serverInfo.itens = Protocol_read(receivedProtocol)
    serverInfo.alItens = Protocol_read(receivedProtocol)

    clearLists()

    setupAlStateButton()
    setupItemList()
    setupAutoLootItemList()

    setupClickListeners()
  end
end

function createItemRow(itemID, item, isInAutoLoot)
  local itemRow = g_ui.createWidget('RowListLoot', isInAutoLoot and listPlayerItens or listItens)
  local itemPreview = itemRow:getChildById('itemPreview')
  local itemName = itemRow:getChildById('itemName')
  local button = itemRow:getChildById('addOrRemoveButton')

  itemPreview:setItemId(item.itemid)
  itemName:setText(item.name)
  
  if not isInAutoLoot then
    setupAddButton(itemRow, itemID, item)
    table.insert(listItemViews, itemRow)
  else
    setupeRemoveButton(itemRow, itemID, item)
  end
end

function setupAddButton(itemRow, itemID, item)
  local button = itemRow:getChildById('addOrRemoveButton')
  button:setText("+")
  button:setTooltip("Adicionar ao Auto-Loot")
  button.onClick = function ()
    if listPlayerItens:getChildCount() >= serverInfo.maxAlItens then
        return
    end
    createItemRow(itemID, item, true)
    itemRow:destroy()
    protocolGame:sendExtendedOpcode(AlOpCode, "add "..itemID)
    updatePlayerItensLabel()
  end
end

function setupeRemoveButton(itemRow, itemID, item)
  local button = itemRow:getChildById('addOrRemoveButton')
  button:setText("-")
  button:setTooltip("Remover do Auto-Loot")
  button.onClick = function ()
    createItemRow(itemID, item, false)
    itemRow:destroy()
    protocolGame:sendExtendedOpcode(AlOpCode, "remove "..itemID)
    updatePlayerItensLabel()
    filterList()
  end
end

function setupAlStateButton()
  if serverInfo.alState == 1 then
    alStateBtn:setText('Auto-Loot Online')
    alStateBtn:setTooltip("Clique para desativar o Auto-Loot")
    alStateBtn:setColor('green')
  else
    alStateBtn:setText('Auto-Loot Offline')
    alStateBtn:setTooltip("Clique para ativar o Auto-Loot")
    alStateBtn:setColor('red')
  end
end

function setupItemList()
    for itemID,item in pairs(serverInfo.itens) do
      createItemRow(itemID, item, false)  
    end
end

function setupAutoLootItemList()
  for itemID,item in pairs(serverInfo.alItens) do
    createItemRow(itemID, item, true)  
  end
  updatePlayerItensLabel()
end

function updatePlayerItensLabel()
  playerItensLabel:setText(string.format("Itens no Auto-Loot (%i/%i)", listPlayerItens:getChildCount(), serverInfo.maxAlItens))
end

--MANAGE BUTON CLICKS
function setupClickListeners()
  alStateBtn.onClick = function()
    if serverInfo.alState == 1 then
        serverInfo.alState = 0
        protocolGame:sendExtendedOpcode(AlOpCode, "turnOff")
    else
        serverInfo.alState = 1
        protocolGame:sendExtendedOpcode(AlOpCode, "turnOn")
    end
    setupAlStateButton()
  end

  cleanPlayerItensBtn.onClick = function()
    protocolGame:sendExtendedOpcode(AlOpCode, "clear")
    clearLists()
    protocolGame:sendExtendedOpcode(AlOpCode, "loadAutoLoot")
  end

  searchEditText.onTextChange = function()
    filterList()
	end
end

function filterList()
  local children = listItens:getChildren()
    for i=1,#children do
      children[i]:hide()
    end
    for i=1,#children do
      local txt = children[i]:getChildById("itemName"):getText()
      if string.find(txt:lower(), searchEditText:getText():lower()) or searchEditText:getText() == "" then
        children[i]:show()
      end
    end
end

function clearLists()
  searchEditText:setText("")
  
  if listItens ~= nil then
    listItens:destroyChildren()
  end
  if listPlayerItens ~= nil then
    listPlayerItens:destroyChildren()
  end
end