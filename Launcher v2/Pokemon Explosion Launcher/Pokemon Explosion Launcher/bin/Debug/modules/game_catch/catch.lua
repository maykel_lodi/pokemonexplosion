local CatcherWindow = nil

function init()

  connect(g_game, { 
		onGameEnd = offline,
		onTextMessage = getParams
  })

  CatcherWindow = g_ui.loadUI('catch.otui', modules.game_interface.getRootPanel())
  CatcherWindowHide()
end

function terminate()
  disconnect(g_game, { 
		onGameEnd = offline,
		onTextMessage = getParams 
	})
	
  CatcherWindow:destroy()
end

function offline()
	CatcherWindow:hide()
end

function getParams(mode, text)
  if not g_game.isOnline() then return end
    if mode == MessageModes.Failure then 
        if text:find("%#CatcherWindow") then
          local search = text:explode("@")

          local pb = text:match("pb=(.-),")
          local gb = text:match("gb=(.-),")
          local sb = text:match("sb=(.-),")
          local ub = text:match("ub=(.-),")
          local allballs = tonumber(pb + gb + sb + ub)
          
          CatcherWindow:recursiveGetChildById("ImagePortrait"):setItemId(search[2])
          CatcherWindow:setText("Pokeballs Wasted")
          local actionText = search[3]
          if(search[5] ~= "info") then
            CatcherWindow:setText("Congratulations!")
            actionText = "You catched a " .. search[3]
          end
          
          CatcherWindow:recursiveGetChildById("TotalBalls"):setText("Total balls used: "..allballs)
          CatcherWindow:recursiveGetChildById("PokeName"):setText(actionText)
          CatcherWindow:recursiveGetChildById("CountPB"):setText(pb.." Poke Balls")
          CatcherWindow:recursiveGetChildById("CountGB"):setText(gb.." Great Balls")
          CatcherWindow:recursiveGetChildById("CountSB"):setText(sb.." Super Balls")
          CatcherWindow:recursiveGetChildById("CountUB"):setText(ub.." Ultra Balls")
          CatcherWindow:show()
      end
  end
end

function CatcherWindowHide()
	CatcherWindow:hide()
end
