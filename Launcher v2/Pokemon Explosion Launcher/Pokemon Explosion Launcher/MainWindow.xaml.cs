﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using System.ComponentModel;
using System.IO;
using System.Xml.Linq;
using Ionic.Zip;
using System.Timers;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Linq;
using System.Windows.Documents;
using System.Diagnostics;

namespace Pokemon_Explosion_Launcher
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly BackgroundWorker updateWorker = new BackgroundWorker();
        private  Uri SOUND_AMBIENT_URI = new Uri(@"sounds/pokemon_theme.wav", UriKind.Relative);
        private  Uri SOUND_EFFECT_ENTER_BUTTON_URI = new Uri(@"sounds/button_enter.wav", UriKind.Relative);

        private BitmapImage soundOnIcon = new BitmapImage(new Uri(@"images/sound_on.png", UriKind.Relative));
        private BitmapImage soundOffIcon = new BitmapImage(new Uri(@"images/sound_off.png", UriKind.Relative));
        private BitmapImage openGlActive = new BitmapImage(new Uri(@"images/start_game_opengl_active_square.png", UriKind.Relative));
        private BitmapImage openGlDisabled = new BitmapImage(new Uri(@"images/start_game_opengl_disabled_square.png", UriKind.Relative));
        private BitmapImage dx9Active = new BitmapImage(new Uri(@"images/start_game_dx9_active_square.png", UriKind.Relative));
        private BitmapImage dx9Disabled = new BitmapImage(new Uri(@"images/start_game_dx9_disabled_square.png", UriKind.Relative));
        private BitmapImage btnRetryActive = new BitmapImage(new Uri(@"images/btn_try_again_active.png", UriKind.Relative));
        private BitmapImage btnRetryDisabled = new BitmapImage(new Uri(@"images/btn_try_again_disabled.png", UriKind.Relative));

        private const string GAME_OPENGL_PROCCESS_NAME = "POKEMON EXPLOSION - OpenGL";
        private const string GAME_DX9_PROCCESS_NAME = "POKEMON EXPLOSION - DirectX9";

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        public const int MAX_ATTEMPS = 3;
        public Timer effectStartButtonTimer = new Timer();
        public bool updateFail = false;
        public int attemps = 0;
        public bool isSoundEnabled = true;
        public MediaPlayer mainPlayer = new MediaPlayer();
        public MediaPlayer effectsPlayer = new MediaPlayer();
        

        public MainWindow()
        {
            InitializeComponent();
            updateWorker.DoWork += updateWorker_DoWork;
            updateWorker.RunWorkerCompleted += updateWorker_RunWorkerCompleted;
            updateWorker.ProgressChanged += updateWorker_ProgressChanged;
            updateWorker.WorkerReportsProgress = true;
            updateWorker.RunWorkerAsync();

            mainPlayer.Volume = 0.1;
            effectsPlayer.Volume = 0.5;

            playAmbientsound();
        }

        private void playAmbientsound()
        {
            mainPlayer.Open(SOUND_AMBIENT_URI); //Som ambiente
            mainPlayer.Play();
            mainPlayer.MediaEnded += new EventHandler(Media_Ended);
        }

        private void pauseAmbientMusic()
        {
            mainPlayer.Pause();
        }

        private void startAmbientMusic()
        {
            mainPlayer.Play();
        }

        private void Media_Ended(object sender, EventArgs e)
        {
            mainPlayer.Position = TimeSpan.Zero;
            mainPlayer.Play();
        }

        private void playEffectSound(Uri sound)
        {
            if (isSoundEnabled)
            {
                effectsPlayer.Open(sound);
                effectsPlayer.Play();
            }
        }

        private void Window_ContentRendered(Object sender, EventArgs e)
        {

        }

        //Delete File
        static bool deleteFile(string f)
        {
            try
            {
                File.Delete(f);
                return true;
            }
            catch (IOException)
            {
                return false;
            }
        }

        private void updateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool isOpenGlClientRunning = Process.GetProcessesByName(GAME_OPENGL_PROCCESS_NAME).Any();
            bool isDx9ClientRunning = Process.GetProcessesByName(GAME_DX9_PROCCESS_NAME).Any();
            if (isOpenGlClientRunning || isDx9ClientRunning)
            {
                MessageBox.Show("Para atualizar o jogo, o cliente deve ser fechado!", "Erro");
                updateFail = true;
                return;
            }

            attemps += 1;

            try
            {
                updateWorker.ReportProgress(0, "Checando versões");
                string Server = "http://149.56.163.31/updates/";

                //Defines application root
                string Root = AppDomain.CurrentDomain.BaseDirectory;

                //Make sure version file exists
                FileStream fs = null;
                if (!File.Exists("version"))
                {
                    using (fs = File.Create("version"))
                    {

                    }

                    using (StreamWriter sw = new StreamWriter("version"))
                    {
                        sw.Write("1.0");
                    }
                }
                //checks client version
                string lclVersion;
                using (StreamReader reader = new StreamReader("version"))
                {
                    lclVersion = reader.ReadLine();
                }
                decimal localVersion = decimal.Parse(lclVersion);


                //server's list of updates
                XDocument serverXml = XDocument.Load(@Server + "Updates.xml");

                //The Update Process
                foreach (XElement update in serverXml.Descendants("update"))
                {
                    string version = update.Element("version").Value;
                    string file = update.Element("file").Value;
                    string notes = update.Element("notes").Value;
                    updateWorker.ReportProgress(0, new string[] { "Patch Notes - Version " + version + "\n", notes});

                    decimal serverVersion = decimal.Parse(version);
                    string sUrlToReadFileFrom = Server + file;
                    string sFilePathToWriteFileTo = Root + file;

                    if (serverVersion > localVersion)
                    {
                        updateWorker.ReportProgress(20);
                        Uri url = new Uri(sUrlToReadFileFrom);
                        System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                        System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
                        response.Close();

                        Int64 iSize = response.ContentLength;

                        Int64 iRunningByteTotal = 0;
                        updateWorker.ReportProgress(40, "Baixando arquivo");
                        using (System.Net.WebClient client = new System.Net.WebClient())
                        {
                            using (System.IO.Stream streamRemote = client.OpenRead(new Uri(sUrlToReadFileFrom)))
                            {
                                using (Stream streamLocal = new FileStream(sFilePathToWriteFileTo, FileMode.Create, FileAccess.Write, FileShare.None))
                                {
                                    int iByteSize = 0;
                                    byte[] byteBuffer = new byte[iSize];
                                    while ((iByteSize = streamRemote.Read(byteBuffer, 0, byteBuffer.Length)) > 0)
                                    {
                                        streamLocal.Write(byteBuffer, 0, iByteSize);
                                        iRunningByteTotal += iByteSize;

                                        double dIndex = (double)(iRunningByteTotal);
                                        double dTotal = (double)byteBuffer.Length;
                                        double dProgressPercentage = (dIndex / dTotal);
                                        int iProgressPercentage = (int)(dProgressPercentage * 100);

                                        updateWorker.ReportProgress(iProgressPercentage);
                                    }

                                    streamLocal.Close();
                                }

                                streamRemote.Close();
                            }
                        }

                        updateWorker.ReportProgress(60, "Extraindo arquivos");
                        //unzip
                        using (ZipFile zip = ZipFile.Read(file))
                        {
                            double extractPercent = 60.0;
                            double step = (double) 20 / zip.Entries.Count();
                            foreach (ZipEntry zipFiles in zip)
                            {
                                zipFiles.Extract(Root + "", true);
                                extractPercent += step;
                                updateWorker.ReportProgress((int)extractPercent);
                            }
                            updateWorker.ReportProgress(100);
                        }

                        updateWorker.ReportProgress(80, "Atualizando controle de versão");
                        //Rewrite new version
                        File.WriteAllText("version", version);

                        //Delete Zip File
                        deleteFile(file);
                    }
                }
            }
            catch (Exception ex)
            {
                updateWorker.ReportProgress(0);
                updateFail = true;
            }
        }


        private void updateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (updateFail)
            {
                if (attemps < MAX_ATTEMPS)
                {
                    statusLabel.Foreground = Brushes.IndianRed;
                    statusLabel.Text = "Falha na atualização";
                    Storyboard btnRetryAnimation = this.FindResource("showRetryButtonAnimation") as Storyboard;
                    Storyboard.SetTarget(btnRetryAnimation, btnRetry);
                    btnRetry.Visibility = Visibility.Visible;
                    btnRetryAnimation.Begin();
                }
                else
                {
                    statusLabel.Foreground = Brushes.IndianRed;
                    statusLabel.Text = "Falha na atualização, tente novamente mais tarde";
                }
            }
            else
            {
                loading.Value = 100;
                showStartButton();
                statusLabel.Foreground = Brushes.DarkGreen;
                statusLabel.Text = "Cliente Atualizado!";
            }
        }

        private void showStartButton()
        {
            Storyboard openGlAnimation = this.FindResource("showStartButtonOpenGLAnimation") as Storyboard;
            Storyboard.SetTarget(openGlAnimation, startBtnOpenGL);
            Storyboard dx9Animation = this.FindResource("showStartButtonDx9Animation") as Storyboard;
            Storyboard.SetTarget(dx9Animation, startBtnDx9);
            

            startBtnDx9.Visibility = Visibility.Visible;
            startBtnOpenGL.Visibility = Visibility.Visible;
            openGlAnimation.Begin();
            dx9Animation.Begin();
        }

        private void updateWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                if(e.UserState.GetType() != typeof(string))
                {
                    string[] notes = ToStringArray(e.UserState);
                    patchNotesLabel.Inlines.Add(new Bold(new Run(notes[0])));
                    patchNotesLabel.Inlines.Add(new Run(notes[1].Replace("-", "\uD83E\uDC36 ")));
                }
                else
                {
                    string message = (string)e.UserState;
                    statusLabel.Text = message + " [" + e.ProgressPercentage + "%]";
                }
            }
            else
            {
                string oldText = statusLabel.Text.Substring(0, statusLabel.Text.IndexOf(" ["));
                statusLabel.Text = oldText + " [" + e.ProgressPercentage + "%]";
            }
            loading.Value = e.ProgressPercentage;
        }

        static string[] ToStringArray(object arg)
        {
            var collection = arg as System.Collections.IEnumerable;
            if (collection != null)
            {
                return collection
                  .Cast<object>()
                  .Select(x => x.ToString())
                  .ToArray();
            }

            if (arg == null)
            {
                return new string[] { };
            }

            return new string[] { arg.ToString() };
        }

        void patchNotes_LoadCompleted(object sender, NavigationEventArgs e)
        {
            string script = "document.documentElement.style.overflow ='hidden'";
            WebBrowser wb = (WebBrowser)sender;
            wb.InvokeScript("execScript", new Object[] { script, "JavaScript" });
        }

        private void startBtnOpenGL_MouseEnter(object sender, EventArgs e)
        {
            playEffectSound(SOUND_EFFECT_ENTER_BUTTON_URI);
            startBtnOpenGL.Source = openGlActive;
        }

        private void startBtnOpenGL_MouseLeave(object sender, EventArgs e)
        {
            startBtnOpenGL.Source = openGlDisabled;
        }

        private void startBtnOpenGL_Click(object sender, EventArgs e)
        {
            Process.Start(GAME_OPENGL_PROCCESS_NAME + ".exe", "\\Pokemon Explosion");
            this.Close();
        }

        private void startBtnDx9_MouseEnter(object sender, EventArgs e)
        {
            playEffectSound(SOUND_EFFECT_ENTER_BUTTON_URI);
            startBtnDx9.Source = dx9Active;
        }

        private void startBtnDx9_MouseLeave(object sender, EventArgs e)
        {
            startBtnDx9.Source = dx9Disabled;
        }

        private void startBtnDx9_Click(object sender, EventArgs e)
        {
            Process.Start(GAME_DX9_PROCCESS_NAME + ".exe", "\\Pokemon Explosion");
            this.Close();
        }

        private void btnRetry_MouseEnter(object sender, EventArgs e)
        {
            playEffectSound(SOUND_EFFECT_ENTER_BUTTON_URI);
            btnRetry.Source = btnRetryActive;
        }

        private void btnRetry_MouseLeave(object sender, EventArgs e)
        {
            btnRetry.Source = btnRetryDisabled;
        }

        private void btnRetry_Click(object sender, EventArgs e)
        {
            btnRetry.Visibility = Visibility.Hidden;

            statusLabel.Foreground = Brushes.Black;
            statusLabel.Text = "Procurando Atualizações...";
            updateFail = false;
            patchNotesLabel.Text = "";
            patchNotesLabel.Inlines.Clear();
            updateWorker.RunWorkerAsync();
        }

        private void soundToggle_Click(object sender, EventArgs e)
        {
            if (isSoundEnabled)
            {
                soundToggle.Source = soundOffIcon;
                isSoundEnabled = false;
                pauseAmbientMusic();
            }
            else
            {
                soundToggle.Source = soundOnIcon;
                isSoundEnabled = true;
                startAmbientMusic();
            }
        }
    }
}
