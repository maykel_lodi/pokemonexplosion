local lootList = nil
local AlLootedOpCode = 34
local LootedItemTime = 7000
local defaultWidth = 0
local defaultHeight = 0

function onInit()
    connect(g_game, {
        onGameStart = onOnline,
        onGameEnd = onOffline
    })
    ProtocolGame.registerExtendedOpcode(AlLootedOpCode, function(protocol, opcode, buffer) onLootList(buffer) end)

    lootList = g_ui.loadUI('lootList', modules.game_interface.getRootPanel())

    defaultWidth = lootList:getWidth()
    defaultHeight = lootList:getHeight()

    if (g_game.isOnline()) then
        onOnline()
    end
end

function onTerminate()
    disconnect(g_game, {
        onGameStart = onOnline,
        onGameEnd = onOffline
    })

    ProtocolGame.unregisterExtendedOpcode(AlLootedOpCode)
    lootList:destroy()
end

function hide()
    lootList:hide()
end

function show()
    lootList:show()
end

function reset()
    lootList:destroyChildren()
end

function onLootList(buffer)
    if buffer == nil or buffer == "" then return end
    local itens = string.explode(buffer, "/")
    for i=1,#itens do
        if itens[i] == nil or itens[i] == "" then
            return
        end

        local itemQuantity = 1
        local itemInfo = string.explode(itens[i],":")
        local itemId = itemInfo[1]
        if itemInfo[2] ~= nil then
            itemQuantity = tonumber(itemInfo[2])
        end
        onLootAdd(itemId, itemQuantity)
    end
end

function onLootAdd(itemId, count)
    local icon = g_ui.createWidget('LootItem', lootList)
    icon:setItemId(itemId)

    resize()
    reallocateIcons()

    local label = g_ui.createWidget('CountLabel', icon)
    label:setId(icon:getId() .. 'label')
    label:setText(count)
    label:addAnchor(AnchorHorizontalCenter, 'parent', AnchorHorizontalCenter)
    label:addAnchor(AnchorTop, 'parent', AnchorBottom)

    local finishFunc = function()
        icon:destroy()
        resize()
        reallocateIcons()
    end
    g_effects.fadeOut(icon, LootedItemTime)
    scheduleEvent(finishFunc, LootedItemTime+10)
end

function reallocateIcons()
    local last
    for k, v in pairs(lootList:getChildren()) do
        v:breakAnchors()

        if (last) then
            v:addAnchor(AnchorLeft, last:getId(), AnchorRight)
        else
            v:addAnchor(AnchorLeft, 'parent', AnchorLeft)
        end

        last = v
    end
end

function resize()
    if (lootList:getChildCount() == 0) then
        lootList:resize(defaultWidth, defaultHeight)
        return
    end

    local width = lootList:getPaddingLeft() + lootList:getPaddingRight()
    for k, v in pairs(lootList:getChildren()) do
        width = width + v:getWidth()
    end

    lootList:resize(width, defaultHeight)
end

function onOnline()
    reset()
    show()
end

function onOffline()
    hide()
    reset()
end