al_config = {
  strStatus = 55554, -- Storage para ativar/desativar o auto-loot.
  strItens = 55555, -- Storage que guarda os itens no auto-loot do player
  free_max_slot = 20, -- Caso queira fazer diferente, coloque valores maximos para premium e free.
  premium_max_slot = 40, -- Caso queira fazer diferente, coloque valores maximos para premium e free.
  opcode = 33, -- Opcode de comunica��o com client, n�o mexer aqui.
  lootedOpcode = 34,
  money_ids = {2148,2152,2160}, -- Configure o id das suas moedas, notas, etc de dinheiro.
  itens = {11447,11442,11441,11453,11445,11443,11444,11446,11448,11450,11451,11452,12232,12244,11449,11454,12276,
  12277,12268,12236,12211,12149,12151,12150,12289,12180,12342,12283,12142,12148,12154,12156,12158,12159,12160,12166,
  12167,12169,12172,12174,12178,12186,12187,12189,12191,12193,12194,12195,12197,12198,12199,12204,12205,12208,12209,
  12223,12226,12228,12229,12235,12240,12241,12243,12250,12270,12271,12272,12273,12274,12278,12279,12280,12284,12285,
  12341,12196,12203,12207,13782,13784,12141,12168,13862,13863,13864,13865,13866,13867,13868,13869,13870,13871,13872,
  13873,13874,13875,13876,13877,13878,13879,13880,13881,13882,13883,13884,13885,13886,13887,13888,13889,13890,13891,
  13892,13893,13894,13895,13897,13898,13899,13900,13901,12204,12196,13867,12175,12183,12157,12207,12185,12176,12177,
  12152,12179,12161,12181,12188,12162,12337,12171,12164,2694,12334,12175,12165,12200,12170,12200,12163,12155,12173,12173,
  12182,12286,12185,12184,12282,12288,12192,12201,12202,12745,13783,13785,13789,12206,12745,12240,12153,24148,24149,24150,
  20719,22051,22050,2151,20898,20899,20900,20901,20902,20903,20905,21079,21996,21996,21997,21998,21999,22000,22001,22002,22003,
  20904
  }
}

function manageAutoLootOpcode(cid, buffer)
  print("AutoLoot - Buffer: "..buffer)
  if string.find(buffer, "loadAutoLoot") then
    print("AutoLoot - loadAutoLoot enter, with cid: "..cid)
    sendItemsAL(cid)
    print("AutoLoot - sendItemsAL passed.")
  elseif string.find(buffer, "add ") then
    local itemId = string.gsub(buffer, "add ", "")
    addItemInAL(cid, itemId, false)
  elseif string.find(buffer, "addFromMenu ") then
    local itemId = string.gsub(buffer, "addFromMenu ", "")
    addItemInAL(cid, itemId, true)
  elseif string.find(buffer, "remove ") then
    local itemId = string.gsub(buffer, "remove ", "")
    removeItemFromAl(cid, itemId)
  elseif string.find(buffer, "clear") then
    clearItensAl(cid)
  elseif string.find(buffer, "turnOn") then
    turnAutoLootOn(cid)
  elseif string.find(buffer, "turnOff") then
    turnAutoLootOff(cid)
  end
  return true
end

function isMoney(itemid)
  if isInArray(al_config.money_ids, itemid) then return true end
  return false
end

function convertToServerID(itemId)
  for v=1, #al_config.itens do
    local id = al_config.itens[v]
    if getItemInfo(id).clientId == tonumber(itemId) then
        return id
    end
  end

  return itemId
end

function addItemInAL(cid, itemId, fromMenu)
  if fromMenu then
    itemId = convertToServerID(itemId)
  end

  if not validateItemID(cid, itemId) then return end

  local newStrValue = ","
  local playerItensInAl = getPlayerStorageValue(cid, al_config.strItens)
  local tempItensAl = playerItensInAl
  local itens = {}
  if playerItensInAl ~= nil and playerItensInAl ~= -1 and string.find(playerItensInAl, ",") then
      itens = string.explode(tempItensAl, ",")
      newStrValue = playerItensInAl
  end
  local qtdItensInAl = getAllItemsInAlCount(itens)
  local max = isPremium(cid) and al_config.premium_max_slot or al_config.free_max_slot
  local itemName = getItemNameById(itemId)
  
  if qtdItensInAl >= max then
    return doPlayerPopupFYI(cid, "Atingiu o limite maximo de itens ("..max..").")
  end

  if string.find(newStrValue, tostring(itemId..",")) then
    return doPlayerPopupFYI(cid, "O item '"..itemName.."' j� est� no Auto-Loot.")
  end

  setPlayerStorageValue(cid, al_config.strItens, tostring(newStrValue.. itemId.. ","))
  if fromMenu then
    doPlayerSendTextMessage(cid, 25, "O item "..itemName.." foi adicionado ao Auto-Loot!")
    sendItemsAL(cid)
  end
end

function removeItemFromAl(cid, itemId)
  if not validateItemID(cid, itemId) then return end

  local playerItensInAl = getPlayerStorageValue(cid, al_config.strItens)
  local tempItensAl = playerItensInAl
  local itens = {}
  if playerItensInAl ~= nil and playerItensInAl ~= -1 and string.find(playerItensInAl, ",") then
      itens = string.explode(tempItensAl, ",")
  end
  local itemName = getItemNameById(itemId)
  
  if not isInArray(itens, itemId) then return end

  if playerItensInAl == nil or playerItensInAl == -1 then
    setPlayerStorageValue(cid, al_config.strItens, "")
    return
  end

  local newStringWithoutItem = string.gsub(playerItensInAl, itemId..",", "")
  setPlayerStorageValue(cid, al_config.strItens, newStringWithoutItem)
end

function clearItensAl(cid)
  local playerItensInAl = getPlayerStorageValue(cid, al_config.strItens)
  if playerItensInAl ~= nil and playerItensInAl ~= -1 then
    setPlayerStorageValue(cid, al_config.strItens, "")
  end
end

function getAllItemsInAlCount(itens)
  local count = 0
	for v=1, #itens do
    count = count+1
  end

  return count
end

function getAllItemsInAl(cid)
  local items = {}
  local playerStorage = getPlayerStorageValue(cid, al_config.strItens)
  local tempItensAl = playerStorage
  if playerStorage == -1 or playerStorage == nil or not string.find(playerStorage, ",") then
      return items
  end
  
  local playerAlItens = string.explode(tempItensAl, ",")

  for v=1, #playerAlItens do
    if playerAlItens[v] ~= nil then
      if tonumber(playerAlItens[v]) > 0 then
        table.insert(items, playerAlItens[v])
      end
    end
  end

  return items
end

function sendItemsAL(cid)
  print("AutoLoot - sendItemsAL")
  local protocol = Protocol_create("itens")
  local items = {}
  local itemsInAl = {}
  local maxPlayerAlItens = isPremium(cid) and al_config.premium_max_slot or al_config.free_max_slot

  local tempItensInAl = getAllItemsInAl(cid)
  for a=1, #tempItensInAl do
    local id = tempItensInAl[a]
	  itemsInAl[id] = {}
    itemsInAl[id].name = getItemNameById(id)
    itemsInAl[id].itemid = getItemInfo(tempItensInAl[a]).clientId
  end

  for v=1, #al_config.itens do
    local id = al_config.itens[v]
    if not isInArray(tempItensInAl, id) then
      items[id] = {}
      items[id].name = getItemNameById(id)
      items[id].itemid = getItemInfo(id).clientId
    end
  end

  Protocol_add(protocol, getPlayerStorageValue(cid, al_config.strStatus))
  Protocol_add(protocol, maxPlayerAlItens)
  Protocol_add(protocol, items)
  Protocol_add(protocol, itemsInAl)
  doSendPlayerExtendedOpcode(cid, al_config.opcode, table.tostring(protocol))
end

function turnAutoLootOn(cid)
  if getPlayerStorageValue(cid, al_config.strStatus) <= 0 then
    setPlayerStorageValue(cid, al_config.strStatus, 1)
  end
end

function turnAutoLootOff(cid)
  if getPlayerStorageValue(cid, al_config.strStatus) == 1 then
    setPlayerStorageValue(cid, al_config.strStatus, 0)
  end
end


function validateItemID(cid, itemId)
  if itemId == -1 then
    doPlayerPopupFYI(cid, "Desculpe, o item n�o foi encontrado.")
    return false
  end

  if not isInArray(al_config.itens, itemId) then
    doPlayerPopupFYI(cid, "Desculpe, o item n�o pode ser usado no Auto-Loot.")
    return false
  end

  return true
end

function doStack(cid, itemid, new)
  local count = getPlayerItemCount(cid, itemid)
		if ((count % 100) == 0) then
			return doPlayerAddItemEx(cid, doCreateItemEx(itemid, new), true)
		elseif (count > 100) then
			count = count - (math.floor(count / 100) * 100)
		end
 
		local newCount = count + new
		if (count ~= 0) then
			local find = getPlayerItemById(cid, true, itemid, count).uid
			if (find > 0) then
				doRemoveItem(find)
			else
				newCount = new
			end
		end
 
		if (newCount > 100) then
			for i = 1, math.floor(newCount / 100) do
				doPlayerAddItemEx(cid, doCreateItemEx(itemid, 100), true)
			end
			newCount = (newCount % 100)
		end
		doPlayerAddItemEx(cid, doCreateItemEx(itemid, newCount), true)
end

function doAutoLoot(cid, pos)
  if not isCreature(cid) then return false end
  local items_al = getAllItemsInAl(cid)
  local itemsLootedMsg = ""
  local itemsLooted = ""
  for _=1, 255 do
    pos.stackpos = _
	  local item = getTileThingByPos(pos)
	--if item.uid > 0 and isCorpse(item.uid) then
    if item.uid > 0 and isContainer(item.uid) and getContainerSize(item.uid) then
      local items = getCorpseItems(item)
      for _, it in pairs(items) do
        local itemid = it.itemid
          if isItemMovable(itemid) then
            if isInArray(items_al, itemid) or isMoney(itemid) then
              if isItemStackable(itemid) and (getPlayerItemCount(cid, itemid) > 0) then
                doStack(cid, itemid, it.type)
              else
                local item = doCreateItemEx(itemid, it.type)
                doPlayerAddItemEx(cid, item, true)
              end

              itemsLootedMsg = itemsLootedMsg..""..(tonumber(it.type) > 1 and tonumber(it.type) or "a").." "..getItemNameById(itemid)..", "
              itemsLooted = itemsLooted..""..getItemInfo(itemid).clientId..":"..tonumber(it.type).."/"
              doRemoveItem(it.uid)
            end
          end
        end
        if itemsLooted ~= nil and itemsLooted ~= "" then
          local fixedMsg = itemsLootedMsg:sub(1,-3)
          doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, 'Auto-Loot: '..fixedMsg..'.')
          doSendPlayerExtendedOpcode(cid, al_config.lootedOpcode, itemsLooted)
        end
      break
    end
  end
end

function getCorpseItems(corpse) --*
  local items = {}
  local containers = {}
  for slot = 0, getContainerSize(corpse.uid)-1 do
    local item = getContainerItem(corpse.uid, slot)
    if item.itemid == 0 then break end
    if isContainer(item.uid) then 
      table.insert(containers, item) 
    end
    table.insert(items, item)
  end
  if #containers > 0 then
    for _, item in ipairs(getCorpseItems(containers[1])) do 
      table.insert(items, item) 
    end
    table.remove(containers, 1)
  end
  return items
end


function getItemsInContainerById(container, itemid) -- Function By Kydrai
	local items = {}
	if isContainer(container) and getContainerSize(container) > 0 then
		for slot=0, (getContainerSize(container)-1) do
			local item = getContainerItem(container, slot)
			if isContainer(item.uid) then
				local itemsbag = getItemsInContainerById(item.uid, itemid)
				for i=0, #itemsbag do
					table.insert(items, itemsbag[i])
				end
			else
				if itemid == item.itemid then
					table.insert(items, item.uid)
				end
			end
		end
	end
	return items
end

function doPlayerAddItemStacking(cid, itemid, amount) --*
	local item, count = getItemsInContainerById(getPlayerSlotItem(cid, 3).uid, itemid), 0
	if #item > 0 then
		for _ ,x in pairs(item) do
			local ret = getThing(x)
			if ret.type < 100 then
				doTransformItem(ret.uid, itemid, ret.type+amount) 
				if ret.type+amount > 100 then
					doPlayerAddItem(cid, itemid, ret.type+amount-100)
				end
				break
			else
				count = count+1
			end
		end
		if count == #item then
			doPlayerAddItem(cid, itemid, amount)
		end
	else
		return doPlayerAddItem(cid, itemid, amount)
	end
end

killua_functions = 
{
  filtrateString = function(str) -- By Killua
      local tb, x, old, last = {}, 0, 0, 0
      local first, second, final = 0, 0, 0
      if type(str) ~= "string" then
          return tb
      end
      for i = 2, #str-1 do
        if string.byte(str:sub(i,i)) == string.byte(':') then
            x, second, last = x+1, i-1, i+2
            for t = last,#str-1 do
              if string.byte(str:sub(t,t)) == string.byte(',') then
                  first = x == 1 and 2 or old
                  old, final = t+2, t-1
                  local index, var = str:sub(first,second), str:sub(last,final)
                  tb[tonumber(index) or tostring(index)] = tonumber(var) or tostring(var)
                  break
              end
            end
        end
      end
      return tb
  end,
  
  translateIntoString = function(tb) -- By Killua
      local str = ""
      if type(tb) ~= "table" then
        return str
      end
      for i, t in pairs(tb) do
        str = str..i..": "..t..", "
      end
      str = "a"..str.."a"
      return tostring(str)
  end
}

function setPlayerTableStorage(cid, key, value)
  return doPlayerSetStorageValue(cid, key, killua_functions.translateIntoString(value))
end

function getPlayerTableStorage(cid, key)
  return killua_functions.filtrateString(getPlayerStorageValue(cid, key))
end