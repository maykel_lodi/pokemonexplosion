failmsgs = {
"Sorry, you didn't catch that pokemon.",
"Sorry, your pokeball broke.",
"Sorry, the pokemon escaped.",
}

function doSendPokeBall(cid, catchinfo, showmsg, fullmsg, typeee)

	local name = catchinfo.name
	local pos = catchinfo.topos
	local topos = {}
		topos.x = pos.x
		topos.y = pos.y
		topos.z = pos.z
	local newid = catchinfo.newid
	local catch = catchinfo.catch
	local fail = catchinfo.fail
	local rate = catchinfo.rate
	local basechance = catchinfo.chance
	
	if pokes[getPlayerStorageValue(cid, 854788)] and name == getPlayerStorageValue(cid, 854788) then 
	   rate = 85
    end

	local corpse = getTopCorpse(topos).uid

	if not isCreature(cid) then
		doSendMagicEffect(topos, CONST_ME_POFF)
		return true
	end

	doItemSetAttribute(corpse, "catching", 1)

	local level = getItemAttribute(corpse, "level") or 0
	local levelChance = level * 0.02

	local totalChance = math.ceil(basechance * (1.2 + levelChance))
	local thisChance = math.random(0, totalChance)
	local myChance = math.random(0, totalChance)
	local chance = (1 * rate + 1) / totalChance
		chance = doMathDecimal(chance * 100)

	if rate >= totalChance then
		local status = {}
		      status.gender = getItemAttribute(corpse, "gender")
		      status.happy = 500

		doRemoveItem(corpse, 1)
		doSendMagicEffect(topos, catch)
		addEvent(doCapturePokemon, 3000, cid, name, newid, status, typeee)  
	return true
	end


	if totalChance <= 1 then totalChance = 1 end

	local myChances = {}
	local catchChances = {}


	for cC = 0, totalChance do
		table.insert(catchChances, cC)
	end

	for mM = 1, rate do
		local element = catchChances[math.random(1, #catchChances)]
		table.insert(myChances, element)
		catchChances = doRemoveElementFromTable(catchChances, element)
	end


	local status = {}
	      status.gender = getItemAttribute(corpse, "gender")
	      status.happy = 500

	doRemoveItem(corpse, 1)

	local doCatch = false

	for check = 1, #myChances do
		if thisChance == myChances[check] then
			doCatch = true
		end
	end

	if doCatch then
		doSendMagicEffect(topos, catch)
		addEvent(doCapturePokemon, 3000, cid, name, newid, status, typeee) 
	else
		addEvent(doNotCapturePokemon, 3000, cid, name, typeee) 
		doSendMagicEffect(topos, fail)
	end
end

function doCapturePokemon(cid, poke, ballid, status, typeee)  
	if not isCreature(cid) then
		return true
	end
	
	local list = getCatchList(cid)
    if not isInArray(list, poke) and not isShinyName(poke) then    
       doPlayerAddSoul(cid, 1)
    end

	doAddPokemonInOwnList(cid, poke)
	CW_Count(cid, poke, typeee)
    CW_Caught(cid, poke)

	if pokes[poke] then
		local test = io.open("data/catch.txt", "a+")
		local read = ""

		if test then
			read = test:read("*all")
			test:close()
		end

		if string.find(poke, "Shiny") then
			read = read.."\n\n\nName: "..getCreatureName(cid).." - Pok�mon: "..poke..""
		else
			read = read.."\nName: "..getCreatureName(cid).." - Pok�mon: "..poke..""
		end
		local reopen = io.open("data/catch.txt", "w")
		reopen:write(read)
		reopen:close()
	end

	if icons[poke] then
		ballid = icons[poke].on
	end	
    
	local description = "Contains a "..poke.."."

	local gender = status.gender
	local happy = 200
                                                   --alterado v1.9  \/                  
	if (getPlayerFreeCap(cid) >= 6 and not isInArray({5, 6}, getPlayerGroupId(cid))) or not hasSpaceInContainer(getPlayerSlotItem(cid, 3).uid) then 
		item = doCreateItemEx(ballid-1)
	else
		item = addItemInFreeBag(getPlayerSlotItem(cid, 3).uid, ballid, 1) 
	end

	doItemSetAttribute(item, "poke", poke)
	doItemSetAttribute(item, "hp", 1)
	doItemSetAttribute(item, "happy", happy)
	-- doItemSetAttribute(item, "gender", gender)
	doItemSetAttribute(item, "fakedesc", description)
	doItemSetAttribute(item, "description", description)
	doItemSetAttribute(item, "tadport", fotos[poke])
	
	
	doItemSetAttribute(item, "addon", 0)
	if poke == "Hitmonchan" or poke == "Shiny Hitmonchan" then    
		doItemSetAttribute(item, "hands", 0)
	end
	----------- task clan ---------------------
	if pokes[getPlayerStorageValue(cid, 854788)] and poke == getPlayerStorageValue(cid, 854788) then
		sendMsgToPlayer(cid, 27, "Quest Done!")
		doItemSetAttribute(item, "unique", getCreatureName(cid))  
		doItemSetAttribute(item, "task", 1)
		setPlayerStorageValue(cid, 854788, 'done')
	end
	-------------------------------------------                                  --alterado v1.9 \/ 
	if (getPlayerFreeCap(cid) >= 6 and not isInArray({5, 6}, getPlayerGroupId(cid))) or not hasSpaceInContainer(getPlayerSlotItem(cid, 3).uid) then   
		doPlayerSendMailByName(getCreatureName(cid), item, 1)   
		doPlayerSendTextMessage(cid, 25, "Congratulations, you caught a pokemon ("..poke..")!")
		doPlayerSendTextMessage(cid, 27, "Congratulations, you caught a pokemon ("..poke..")!")
		doPlayerSendTextMessage(cid, 27, "Since you are already holding six pokemons, this pokeball has been sent to your depot.")     
		-- doBroadcastMessage("O jogador "..getCreatureName(cid).." acabou de capturar um "..poke..". Parab�ns!")
	else
		doPlayerSendTextMessage(cid, 25, "Congratulations, you caught a ("..poke..")!")
		-- doBroadcastMessage("O jogador "..getCreatureName(cid).." acabou de capturar um "..poke..". Parab�ns!")
		doUpdatePokemonsBar(cid)
	end

	if #getCreatureSummons(cid) >= 1 then
		doSendMagicEffect(getThingPos(getCreatureSummons(cid)[1]), 173) 
			if catchMakesPokemonHappier then
				setPlayerStorageValue(getCreatureSummons(cid)[1], 1008, getPlayerStorageValue(getCreatureSummons(cid)[1], 1008) + 20)
				if useOTClient then
				doCreatureExecuteTalkAction(cid, "/salvar")
	end
			end
	else
		doSendMagicEffect(getThingPos(cid), 173) 
	end

	doIncreaseStatistics(poke, true, true)

end

function doNotCapturePokemon(cid, poke, typeee)  

	if not isCreature(cid) then
	return true
	end

	doPlayerSendTextMessage(cid, 27, failmsgs[math.random(#failmsgs)])

	if #getCreatureSummons(cid) >= 1 then
		doSendMagicEffect(getThingPos(getCreatureSummons(cid)[1]), 166)
	else
		doSendMagicEffect(getThingPos(cid), 166)
	end
	
local storage = newpokedex[poke].stoCatch
--doBrokesCount(cid, storage, typeee)   
doIncreaseStatistics(poke, true, false)
CW_Count(cid, poke, typeee)
end



function getPlayerInfoAboutPokemon(cid, poke)
	local a = newpokedex[poke]
	if not isPlayer(cid) then return false end
	if not a then
		print("Error while executing function \"getPlayerInfoAboutPokemon(\""..getCreatureName(cid)..", "..poke..")\", "..poke.." doesn't exist.")
	return false
	end
	local b = getPlayerStorageValue(cid, a.storage)

	if b == -1 then
		setPlayerStorageValue(cid, a.storage, poke..":")
	end

	local ret = {}
		if string.find(b, "catch,") then
			ret.catch = true
		else
			ret.catch = false
		end
		if string.find(b, "dex,") then
			ret.dex = true
		else
			ret.dex = false
		end
		if string.find(b, "use,") then
			ret.use = true
		else
			ret.use = false
		end
return ret
end


function doAddPokemonInOwnList(cid, poke)

	if getPlayerInfoAboutPokemon(cid, poke).use then return true end

	local a = newpokedex[poke]
	local b = getPlayerStorageValue(cid, a.storage)

	setPlayerStorageValue(cid, a.storage, b.." use,")
end

function isPokemonInOwnList(cid, poke)

	if getPlayerInfoAboutPokemon(cid, poke).use then return true end

return false
end

function getCatchList(cid)

local ret = {}

for a = 1000, 1251 do
	local b = getPlayerStorageValue(cid, a)
	if b ~= 1 and string.find(b, "catch,") then
		table.insert(ret, oldpokedex[a-1000][1])
	end
end

return ret

end


function getStatistics(pokemon, tries, success)

local ret1 = 0
local ret2 = 0

	local poke = ""..string.upper(string.sub(pokemon, 1, 1))..""..string.lower(string.sub(pokemon, 2, 30))..""
	local dir = "data/Pokemon Statistics/"..poke.." Attempts.txt"
	local arq = io.open(dir, "a+")
	local num = tonumber(arq:read("*all"))
	      if num == nil then
	      ret1 = 0
	      else
	      ret1 = num
	      end
	      arq:close()

	local dir = "data/Pokemon Statistics/"..poke.." Catches.txt"
	local arq = io.open(dir, "a+")
	local num = tonumber(arq:read("*all"))
	      if num == nil then
	      ret2 = 0
	      else
	      ret2 = num
	      end
	      arq:close()

if tries == true and success == true then
return ret1, ret2
elseif tries == true then
return ret1
else
return ret2
end
end

function doIncreaseStatistics(pokemon, tries, success)

local poke = ""..string.upper(string.sub(pokemon, 1, 1))..""..string.lower(string.sub(pokemon, 2, 30))..""

	if tries == true then
		local dir = "data/Pokemon Statistics/"..poke.." Attempts.txt"

		local arq = io.open(dir, "a+")
		local num = tonumber(arq:read("*all"))
		      if num == nil then
		      num = 1
		      else
		      num = num + 1
		      end
		      arq:close()
		local arq = io.open(dir, "w")
		      arq:write(""..num.."")
		      arq:close()
	end

	if success == true then
		local dir = "data/Pokemon Statistics/"..poke.." Catches.txt"

		local arq = io.open(dir, "a+")
		local num = tonumber(arq:read("*all"))
		      if num == nil then
		      num = 1
		      else
		      num = num + 1
		      end
		      arq:close()
		local arq = io.open(dir, "w")
		      arq:write(""..num.."")
		      arq:close()
	end
end

function doUpdateGeneralStatistics()
	
	local dir = "data/Pokemon Statistics/Pokemon Statistics.txt"
	local base = "NUMBER  NAME        TRIES / CATCHES\n\n"
	local str = ""

for a = 1, 251 do
	if string.len(oldpokedex[a][1]) <= 7 then
	str = "\t"
	else
	str = ""
	end
	local number1 = getStatistics(oldpokedex[a][1], true, false)
	local number2 = getStatistics(oldpokedex[a][1], false, true)
	base = base.."["..threeNumbers(a).."]\t"..oldpokedex[a][1].."\t"..str..""..number1.." / "..number2.."\n"
end
	
	local arq = io.open(dir, "w")
	      arq:write(base)
 	      arq:close()
end

function getGeneralStatistics()
	
	local dir = "data/Pokemon Statistics/Pokemon Statistics.txt"
	local base = "Number/Name/Tries/Catches\n\n"
	local str = ""

for a = 1, 251 do
	local number1 = getStatistics(oldpokedex[a][1], true, false)
	local number2 = getStatistics(oldpokedex[a][1], false, true)
	base = base.."["..threeNumbers(a).."] "..oldpokedex[a][1].."  "..str..""..number1.." / "..number2.."\n"
end
	
return base
end

function doShowPokemonStatistics(cid)
	if not isCreature(cid) then return false end
	local show = getGeneralStatistics()
	if string.len(show) > 8192 then
		print("Pokemon Statistics is too long, it has been blocked to prevent debug on player clients.")
		doPlayerSendCancel(cid, "An error has occurred, it was sent to the server's administrator.") 
	return false
	end
	doShowTextDialog(cid, math.random(2391, 2394), show)
end  