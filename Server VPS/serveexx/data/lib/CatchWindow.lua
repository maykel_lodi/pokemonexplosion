CW_Balls = {
	["normal"] = "pb", -- PokeBall
	["great"] = "gb", -- GreatBall
	["super"] = "sb", -- SuperBall
	["ultra"] = "ub", -- UltraBall
	["mega"] = "mb", -- MegaBall
	["dark"] = "db", -- DarkBall
}

CW_DEFAULT_STORAGE = "pb=0,gb=0,sb=0,ub=0,mb=0,db=0," --Valor padr�o quando n�o usou nenhuma ball ainda.

-------- Functions ----------
function CW_Count(cid, pokeName, ball)
	local name = nil
	if tostring(pokeName) then
		name = pokeName
	else
		name = getCreatureName(pokeName)
	end

	local pk = newpokedex[name]

	if pk then
		if haveToReset(getPlayerStorageValue(cid, pk.stoCatch)) then
			setPlayerStorageValue(cid, pk.stoCatch, CW_DEFAULT_STORAGE)
		end

		local txt = getPlayerStorageValue(cid, pk.stoCatch)
		local change = txt:match(CW_Balls[ball].."=(.-),")
		local num = tonumber(change)

		local ret = string.gsub(txt, CW_Balls[ball].."="..num, CW_Balls[ball].."="..(num+1))
		return setPlayerStorageValue(cid, pk.stoCatch, ret) and true
	end
end

function haveToReset(storage)
	if(storage == nil or storage == -1) then
		return true
	end

	if storage:find("normal") then
		return true
	end

	return false
end

function CW_Caught(cid, pokeName)
	local name = nil
	if tostring(pokeName) then
		name = pokeName
	else
		name = getCreatureName(pokeName)
	end

	local pk = newpokedex[name]

	if pk then
		local pokeStorage = getPlayerStorageValue(cid, pk.storage)
		doPlayerSendTextMessage(cid, 27, "CW_Caught -> pokeStorage: " .. pokeStorage)

		if pokeStorage == -1 or not string.find(pokeStorage, "catch,") then
			doPlayerSendCancel(cid, "%#CatcherWindow@"..getItemInfo(fotos[name]).clientId.."@"..pokeName.."@"..getPlayerStorageValue(cid, pk.stoCatch).."@catch")

			local newPokeStorage = ""
			if pokeStorage == -1 then
				newPokeStorage = name..":"
			end
			if not string.find(pokeStorage, "catch,") then
				newPokeStorage = newPokeStorage.." catch,"
			end
			setPlayerStorageValue(cid, pk.storage, newPokeStorage)
		end

		setPlayerStorageValue(cid, pk.stoCatch, CW_DEFAULT_STORAGE)
	end
	return true
end

function CW_PokemonWastedInfo(cid, pokeName)
	local name = nil
	if tostring(pokeName) then
		name = pokeName
	else
		name = getCreatureName(pokeName)
	end

	local pk = newpokedex[name]

	if pk then
		if haveToReset(getPlayerStorageValue(cid, pk.stoCatch)) then
			setPlayerStorageValue(cid, pk.stoCatch, CW_DEFAULT_STORAGE)
		end

		doPlayerSendCancel(cid, "%#CatcherWindow@"..getItemInfo(fotos[name]).clientId.."@"..pokeName.."@"..getPlayerStorageValue(cid, pk.stoCatch).."@info")
		doPlayerSendCancel(cid, "")
	else
		doPlayerSendTextMessage(cid, 27, "Pokemon n�o encontrado")
	end
	return true
end