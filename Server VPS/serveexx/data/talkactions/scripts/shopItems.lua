local config = {
diamond_id = 2145,
virusPotion = {sto = 99999970, itemID = 20897, price = 30, effect = 222, msg = "Voce comprou uma Virus Potion."}
}


function onSay(cid, words, param)

	if(words == "#virusPotion#") then
		if getPlayerItemCount(cid, config.diamond_id) >= config.virusPotion.price then
			doPlayerRemoveItem(cid, config.diamond_id, config.virusPotion.price)
			doPlayerAddItem(cid,  config.virusPotion.itemID, 1)
			doSendMagicEffect(getCreaturePosition(cid), config.virusPotion.effect)
			doPlayerSendTextMessage(cid, 27, config.virusPotion.msg)
		else
			doPlayerSendTextMessage(cid, 27, "Voce nao tem diamonds o suficiente.")
		end

	end

	return TRUE
end