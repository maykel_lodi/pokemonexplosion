function onSay(cid, words, param)

   if(param == '') then
      doPlayerSendTextMessage(cid, 27, "Coloque o nome de um pokemon...")
      return true
   end

   CW_PokemonWastedInfo(cid, doCorrectPokemonName(param))
   return true
end