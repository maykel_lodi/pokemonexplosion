local config = {
    positions = {
		["VIP"] = { x = 1059, y = 1042, z = 7 },
		["Staff!"] = { x = 1051, y = 1042, z = 6 },
		["Leaf Valey"] = { x = 1027, y = 1062, z = 13 },
		["Fire Valey"] = { x = 1039, y = 1049, z = 13 },
                ["Area PvP!"] = { x = 1072, y = 1046, z = 7 },
		["Water Valey"] = { x = 1055, y = 1061, z = 13 },
		["Clique Aqui!"] = { x = 151, y = 125, z = 6 },
		["Bem Vindo!"] = { x = 151, y = 133, z = 6 },
                ["Tower!"] = { x = 1053, y = 1042, z = 6 },
                ["Vally!"] = { x = 1049, y = 1042, z = 6 },
		["Pesca!"] = { x = 1063, y = 1042, z = 7 },
		["Atencao!!!"] = { x = 267, y = 490, z = 8 },
		["Atencao!!"] = { x = 265, y = 490, z = 8 },
		["Tasks"] = { x = 652, y = 1221, z = 7 },
		["Boost"] = { x = 651, y = 1222, z = 7 },
		["Boost!"] = { x = 1065, y = 1042, z = 7 },
		["TC!"] = { x = 1061, y = 1042, z = 7 },
		["Held Converter"] = { x = 960, y = 1063, z = 13 },
		["Held Fusion"] = { x = 955, y = 1063, z = 13 },
		["Held Machine"] = { x = 954, y = 1065, z = 13 },
		}

}

function onThink(cid, interval, lastExecution)
    for text, pos in pairs(config.positions) do
        doSendAnimatedText(pos, text, math.random(1, 255))
    end
    
    return TRUE
end  