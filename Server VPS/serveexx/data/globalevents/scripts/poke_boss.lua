local bossEventStr = 78341
local config = {
	["21:13"] = {
		name = "Salve o mundo!", 
		monsters = {
			{pokemon="Charizard",pos={x=1056, y=1058, z=7}},
			{pokemon="Charizard", pos={x=1056, y=1058, z=7}}
		},
		liveTime=2,
		alertTime=1
	}
}
	
function onThink(interval, lastExecution)
	hours = tostring(os.date("%X")):sub(1, 5)
	local event = config[hours]

	local hoursFixedNumber = string.gsub(hours, ":", "")
	if event and getGlobalStorageValue(bossEventStr) ~= tonumber(hoursFixedNumber) then
		doBroadcastMessage(hours .. " - " .. event.name .. " foi iniciado.")
		setGlobalStorageValue(bossEventStr, hoursFixedNumber)

		local monstersCreated = {} 
		for _,x in pairs(event.monsters) do
			local pk = doSummonCreature(x.pokemon, x.pos)
			if pk ~= nil then
				table.insert(monstersCreated, pk)
			end
		end

		if table.getn(monstersCreated) > 0 then
			if event.liveTime > 0 then
				addEvent(doRemovePokemonBossEvent, event.liveTime*60*1000, event.name, monstersCreated)
			end
			if event.alertTime > 0 then
				addEvent(alertBossEndTime, (event.liveTime-event.alertTime)*60*1000, event.name)
			end
		end
	end
	return true
end

function doRemovePokemonBossEvent(event, listPk)
	for _,pk in pairs(listPk) do
		if isCreature(pk) then
			doRemoveCreature(pk)
		end
	end
	
	doBroadcastMessage("O evento '"..event.."' terminou.")
end

function alertBossEndTime(event)
	doBroadcastMessage("O evento '"..event.."' termina em 1 minuto.")
end