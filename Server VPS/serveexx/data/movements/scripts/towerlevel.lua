
levelToTower = 500

function onStepIn(cid, item, position, fromPosition)
    if getPlayerLevel(cid) < levelToTower then
        doTeleportThing(cid, fromPosition, true)
        doSendMagicEffect(getThingPos(cid), CONST_ME_MAGIC_RED)
        doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_RED, "Somente level " .. levelToTower .. " ou mais podem subir na torre!")
    end
    return TRUE
end